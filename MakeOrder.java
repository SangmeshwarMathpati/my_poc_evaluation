package my_poc;

import java.util.Scanner;

public class MakeOrder {

	OrderData od=new OrderData();
	void makeOrder()
	{
		int id;
		Scanner s=new Scanner(System.in);
		System.out.println("-----BIKE----- \n");
		System.out.println("ID : 1] Hero Xpulse 200 ");
		System.out.println("ID : 2] Honda CB Hornet ");
		System.out.println("ID : 3] TVS Apache RTR ");
		System.out.println("ID : 4] Honda X Blade ");
		System.out.println("ID : 5] Bajaj Pulsar 150 ");
		
		System.out.println("-----CAR---- \n");
		System.out.println("ID : 6] Maruti Suzuki Swift ");
		System.out.println("ID : 7] KIA Seltos ");
		System.out.println("ID : 8] KIA Sonet ");
		System.out.println("ID : 9] Maruti Suzuki Baleno ");
		System.out.println("ID : 10] TATA Nexon ");
		
		System.out.println("-----BiCycle---- \n");
		System.out.println("ID : 11] Hercules Defender ");
		System.out.println("ID : 12] Hero Next ");
		System.out.println("ID : 13] Kross K ");
		System.out.println("ID : 14] Modern LX ");
		System.out.println("ID : 15] OMO Model ");
		
		System.out.print("Enter ID To Make an Order...\n");
		id=s.nextInt();
		System.out.println("Congrats!!!!....Your Order IS Placed \n");
		System.out.println("Details are : \n");
		switch(id) {
		case 1:
			od.xpulse();
			break;
		case 2:
			od.cbhornet();
			break;
		case 3:
			od.apache();
			break;
		case 4:
			od.xblade();
			break;
		case 5:
			od.pulsar();
			break;
			
		case 6:
			od.swift();
			break;
		case 7:
			od.seltos();
			break;
		case 8:
			od.sonet();
			break;
		case 9:
			od.baleno();
			break;
		case 10:
			od.nexon();
			break;
			
		case 11:
			od.hercules();
			break;
		case 12:
			od.hero();
			break;
		case 13:
			od.kross();
			break;
		case 14:
			od.modern();
			break;
		case 15:
		od.omo();
			break;
		default:{
			System.out.println("Please Select Valid Choice....!!!\n");
			MyPocMain MP=new MyPocMain();
			MP.mainMethod();
		}

		
	
	}
	}
}
