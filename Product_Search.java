package my_poc;
import java.util.*;
public class Product_Search {

	void product_Search()
	{	
		Scanner s=new Scanner(System.in);
		MyPocMain MP=new MyPocMain();
		System.out.println("**********Product Search Page.....**********");
		System.out.println();
		System.out.println("1>> Bike");
		System.out.println("2>> Car");
		System.out.println("3>> BiCycle");
		System.out.println("Please Select Choice Number :");	
		int ch=s.nextInt();
		//Use of Switch-Case for Selection.
		switch(ch){
			case 1:
				Bike bk=new Bike();
				bk.bike();
				System.out.println("");
				MP.mainMethod();
				break;

			case 2:
				Car car=new Car();
				car.car();
				System.out.println("");
				MP.mainMethod();
				break;

			case 3:
				BiCycle bcycle=new BiCycle();
				bcycle.bicycle();
				System.out.println("");
				MP.mainMethod();
				break;
			default:
			{
				System.out.println("Please Select Valid Choice....!!!");
				MP.mainMethod();
			}
				
		}
	}
}
	


