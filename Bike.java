package my_poc;
import java.util.*;

// Implementation of Vehicle_Interface.

public class Bike implements Vehicle_Interface {

	// Method() definition.
	public void bike()
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Welcome To The Bike Dept....");
		System.out.println(".....Available Products....\n");
		
		// Use of string array.	
		String bike[]= {"1] Hero Xpulse 200","2] Honda CB Hornet","3] TVS Apache RTR","4] Honda X Blade","5] Bajaj Pulsar 150"};
		
		// Use of for loop to print array data.
		for(int i=0;i<bike.length;i++)
		{
			System.out.println(bike[i]);
		}
			
		System.out.println("Please Select Choice Number To See Detail:");
		int ch1=s.nextInt();
		AllData ad=new AllData();
		
		// Use of switch-case.
		switch(ch1)
		{
			case 1:
				ad.xpulse();
				break;
			case 2:
				ad.cbhornet();
				break;
			case 3:
				ad.apache();
				break;
			case 4:
				ad.xblade();
				break;
			case 5:
				ad.pulsar();
				break;
			default:
			{
				System.out.println("Please Select Valid Choice....!!!\n");
				MyPocMain MP=new MyPocMain();
				MP.mainMethod();
			}
		}
	}
	@Override
	public void car() {
		// TODO Auto-generated method stub	
	}
	@Override
	public void bicycle() {
		// TODO Auto-generated method stub
	}
}
