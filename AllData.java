package my_poc;

public class AllData {
// Use of methods: 
// To wrap the data.
// To achieve encapsulation.
// To achieve code reuse.
	void xpulse()
	{
		System.out.println("Product Name: Hero Xpulse 200");
		System.out.println("Price : 97,000");
		System.out.println("Milage : 40 KMPL");
		System.out.println("Deposit Amount: 30,000");
		System.out.println("Seller Information:ABC Hero Pune,Ridewell Motors Nanded,XYZ HeroMotors Delhi");
		System.out.println("Overall Rating : 4.0");
	
	}
	void cbhornet() 
	{
		System.out.println("Product Name: Honda CB Hornet");
		System.out.println("Price : 95,000");
		System.out.println("Milage : 60 KMPL");
		System.out.println("Deposit Amount: 28,000");
		System.out.println("Seller Information:ABC Honda Pune,Ridewell Motors Nanded,XYZ HondaMotors Delhi");
		System.out.println("Overall Rating : 4.1");
	}
	void apache()
	{
		System.out.println("Product Name: TVS Apache RTR");
		System.out.println("Price : 94,000");
		System.out.println("Milage : 50 KMPL");
		System.out.println("Deposit Amount: 32,000");
		System.out.println("Seller Information:ABC TVS Pune,TVS Motors Nanded,XYZ TVSMotors Delhi");
		System.out.println("Overall Rating : 4.3");
			
	}
	void xblade()
	{
		System.out.println("Product Name: Honda X Blade");
		System.out.println("Price : 80,823");
		System.out.println("Milage : 50 KMPL");
		System.out.println("Deposit Amount: 29,000");
		System.out.println("Seller Information:Ram Honda Pune,Honda Motors Nanded,XYZ HondaMotors Delhi");
		System.out.println("Overall Rating : 4.1");
	
	}
	void pulsar()
	{
		System.out.println("Product Name: Bajaj Pulsar 150");
		System.out.println("Price : 80,823");
		System.out.println("Milage : 50 KMPL");
		System.out.println("Deposit Amount: 29,000");
		System.out.println("Seller Information:Ram Honda Pune,Honda Motors Nanded,XYZ HondaMotors Delhi");
		System.out.println("Overall Rating : 4.4");

	}
	void swift()
	{
		System.out.println("Product Name: Maruti Suzuki Swift");
		System.out.println("Price : 5.73 Lakh");
		System.out.println("Milage : 23.76 KMPL");
		System.out.println("Deposit Amount: 1 Lakh");
		System.out.println("Seller Information:Maruti Suzuki Hydrabad,Ridewell Motors Nanded,XYZ Cars Delhi");
		System.out.println("Overall Rating : 4.6");
	
	}
	
	void seltos()
	{
		System.out.println("Product Name: KIA Seltos");
		System.out.println("Price : 9.90 Lakhs");
		System.out.println("Milage : 20.5 KMPL");
		System.out.println("Deposit Amount: 3 Lakhs");
		System.out.println("Seller Information:Sony Seltos Pune,Seltos Motors Nanded,Rock Seltos Delhi");
		System.out.println("Overall Rating : 4.2");
	
	}
	void sonet()
	{
		System.out.println("Product Name: KIA Sonet");
		System.out.println("Price : 6.79 Lakh");
		System.out.println("Milage : 24.1 KMPL");
		System.out.println("Deposit Amount: 1.3 Lakh");
		System.out.println("Seller Information:Sony Seltos Pune,Seltos Motors Nanded,Rock Seltos Delhi");
		System.out.println("Overall Rating : 4.2");

	}
	void baleno()
	{
		System.out.println("Product Name: Maruti Suzuki Baleno");
		System.out.println("Price : 5.88 Lakh");
		System.out.println("Milage : 27.39 KMPL");
		System.out.println("Deposit Amount: 1.10 Lakh");
		System.out.println("Seller Information:Maruti Suzuki Hydrabad,Ridewell Motors Nanded,XYZ Cars Delhi");
		System.out.println("Overall Rating : 4.3");
	
	}
	void nexon()
	{
		System.out.println("Product Name: TATA Nexon");
		System.out.println("Price : 7.10 Lakh");
		System.out.println("Milage : 22.4 KMPL");
		System.out.println("Deposit Amount: 1.60 Lakh");
		System.out.println("Seller Information:Sun TATA Pune,TATA Motors Nanded,TATA Cars Delhi");
		System.out.println("Overall Rating : 4.4");
	}
	void hercules() {
		System.out.println("Product Name: Hercules Defender");
		System.out.println("Price : 9,499 Rs.");
		System.out.println("Gear:Single Speed");
		System.out.println("Front Brake: Disc Brake");
		System.out.println("Seller Information:Hercules Cycle Hydrabad,SS Cycles Nanded,XYZ Bicycles Delhi");
		System.out.println("Overall Rating : 4.1");
	
	}
	void hero()
	{
		System.out.println("Product Name: Hero Next");
		System.out.println("Price : 9,399 Rs.");
		System.out.println("Gear : 18 Gear");
		System.out.println("Front Brake:Wire Break");
		System.out.println("Seller Information:Sony Hero Pune,Pawan Cycles Nanded,Rock Cycle Delhi");
		System.out.println("Overall Rating : 4.0");	
	
	}
	void kross() {
		System.out.println("Product Name: Kross K");
		System.out.println("Price : 8,299 Rs.");
		System.out.println("Gear : Sible Speed");
		System.out.println("Front Brake:Wire Break");
		System.out.println("Seller Information:SP Bicycle Pune,Pawan Cycles Nanded,Rock Cycle Delhi");
		System.out.println("Overall Rating : 4.8");

	}
	void modern()
	{
		System.out.println("Product Name: Modern LX");
		System.out.println("Price : 9,489 Rs.");
		System.out.println("Suspension : Only front");
		System.out.println("Frame : Steel");
		System.out.println("Seller Information:Ridewell Cycle Hydrabad,MyCycle Nanded,Raj Cycle Delhi");
		System.out.println("Overall Rating : 4.3");
	}
	void omo()
	{
		System.out.println("Product Name: OMO Model");
		System.out.println("Price : 8,595 Rs.");
		System.out.println("Suspension : Rigid");
		System.out.println("Gear : Single Speed");
		System.out.println("Seller Information:Axis Cycle Pune,T Bikepoint Nanded,ABD Bikes Delhi");
		System.out.println("Overall Rating : 4.1");
	}
}
