package my_poc;
import java.util.*;

//Implementation of Vehicle_Interface.
public class Car implements Vehicle_Interface {

	// Method() definition.
	public void car()
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Welcome To The Car Dept....");
		System.out.println(".....Available Products....\n");
		
		// Use of string array.
		String car[]= {"1] Maruti Suzuki Swift","2] KIA Seltos","3] KIA Sonet","4] Maruti Suzuki Baleno","5] TATA Nexon"};
		
		// Use of for loop to print array data.
		for(int i=0;i<car.length;i++)
		{
			System.out.println(car[i]);
		}
		System.out.println("Please Select Choice Number To See Detail:");
		
		int ch2=s.nextInt();
		AllData ad=new AllData();
		
		// Use of switch-case.
		switch(ch2)
		{
			case 1:
				ad.swift();
				break;
			case 2:
				ad.seltos();
				break;
			case 3:
				ad.sonet();	
				break;
			case 4:
				ad.baleno();
				break;
			case 5:
				ad.nexon();
				break;
			default:
			{
				System.out.println("Please Select Valid Choice....!!!\n");
				MyPocMain MP=new MyPocMain();
				MP.mainMethod();
			}
				
		}
	}
	@Override
	public void bike() {
		// TODO Auto-generated method stub
	}
	@Override
	public void bicycle() {
		// TODO Auto-generated method stub
	}
}
