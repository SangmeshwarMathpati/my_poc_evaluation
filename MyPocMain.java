package my_poc;
import java.util.*;
import java.lang.*;
public class MyPocMain
{
	
	public void mainMethod()
	{
		Scanner s=new Scanner(System.in);
		System.out.println("1] Search Products 2] Add Products To Cart 3] Place Your Order 4] Exit \n");
		int choice=s.nextInt();

		if(choice==1) {
			Product_Search PS=new Product_Search();
			PS.product_Search();
		}
		else if(choice==2) {
			AddToCart ATC=new AddToCart();
			ATC.addToCart();
		}
		else if(choice==3)
		{
			MakeOrder mo=new MakeOrder();
			mo.makeOrder();
		}
		else if(choice==4)
		{
			System.out.println("Thank You...");
			System.exit(0);
		}
		
		else {
			System.out.println("Please Select Valid Choice....!!!");
			MyPocMain MP=new MyPocMain();
			MP.mainMethod();
		}
		

	}
	public static void main(String args[])
	{	
	Login lg=new Login();
	lg.login();
	}
}
